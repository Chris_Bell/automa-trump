# Automa-Trump

Automa-Trump is a now-defunct Twitter bot that once tweeted procedurally-generated tweets ~3 times daily based on the actual tweets of Donald J. Trump. The bot has been offline since the Twitter API pricing changes that took many bots like it offline. Since then, the account has been compromised by Bitcoin scammers and mostly tweets spam.

When it was alive, its output ranged from completely nonsensical:

![](https://i.imgur.com/LGsPIKO.jpeg)

to poignant:

![](https://i.imgur.com/GElQ5LV.jpeg)

to believably realistic:

![](https://i.imgur.com/yXGZ1ri.jpeg)

to comedic:

![](https://i.imgur.com/zSTEGFi.jpeg)

It topped out around 200 followers.

## Architecture

Automa-Trump was built on Firebase using the Real-time Data Store and Cloud Functions. In its original implementation, it would pull the latest 200 tweets (the Twitter API limit for a single request) and use them as its data set for generation. However, after Mr. Trump made an oopsie and got [banned from Twitter](https://blog.twitter.com/en_us/topics/company/2020/suspension), this was no longer possible. Thankfully, a random person on the internet (whom I can no longer find) had archived all of Mr. Trump's tweets up to about a month before his ban and made them publicly available in a convenient JSON blob. These were uploaded to the Real-time Data Store and used for source material instead. Automa-Trump's output was less fresh and topical than once before, but limped along for a few more years.

To generate tweets, a Typescript Cloud Function would run 3-4 times a day. Tweets were generated based on the database of archived tweets.

## Tweet Generation

Tweets were generated using a lightly modified [Markov Chain algorithm](https://en.m.wikipedia.org/wiki/Markov_chain):
1. Create a hash map. Each key in the map will be a word in a tweet. The value at that key will be a list of words that have occurred directly after the key in a tweet. e.g. `Map.get('United') => ['States', 'Nations']`
2. For each tweet in the source material:
    1. Split the tweet's contents on `' '`
    2. For each word (which, importantly, will include punctuation) in the tweet:
        1. If it is the first word in a tweet, add it to a special array of "starting words". This will be used later during tweet generation.
        2. Append the following word to the list stored in the hash map for that key. If the current word is the end of the tweet, append `'END'` to signify a tweet termination.
3. Generate a tweet using the map that was generated in step 2:
    1. Pick a random word from the "starting words" array mentioned in step 2.2.1. Using the starting words helps greatly with the coherence of the resulting tweet.
    2. While the tweet is of legal length:
        1. If the current word is `'END'`, break
        2. Append the current word to the tweet
        3. If the current word ends in punctuation (`'.'`, `'?'`, `'!'`, etc.), break
        4. Pick a random next word from `Map.get(currentWord)`
4. If this generated tweet has been posted by the bot before, repeat step 3. Else, post the tweet.


## Optimization

The first problem that needed to be solved after initially implementing the above algorithm (and probably any procedural generation algorithm) is figuring out when to stop. Tweets helpfully have a character limit, but it quickly became clear that longer tweets were scattered and rambling (see example tweet above). Steps 3.2.1 and 3.2.3 were added to help the generation halt before the character limit and (hopefully) yield better tweets.

This helped, but tweets were still very random-sounding. As it turns out, using 200 tweets worth of source material did not lead to coherent text most of the time. Picking a random five out of those 200 to use as source material in step 2 helped a lot with coherence. Instead of complete word salad, tweets began to be mixes of two or three distinct phrases:

 - The vaccines are being delivered to the ~~states...~~
 - ~~... at some point in~~ the future.

 results in
 >The vaccines are being delivered to the future

where "the" is the "crossing point" of the two tweets.

 Tweets on average were much higher quality with this change, but the relatively small data set of five tweets led to occasional loops.

> ... and even more importantly, the Democrats, and even more importantly, ...

Some tweets would even be the same phrase looped until the character limit. This was relatively rare, and was some times quite humorous, so I never got around to fixing it. To fix it, however, an additional check could've been added during step 4 to check for repeated words or pairs of words.


## Future developments
Even before its untimely death, I toyed with several ideas on how to expand this idea beyond a Twitter bot. None of them ever came to fruition.

### Make Your Own Trump Tweet
A web app where you can provide a few words to be included in a Trump-like tweet. Pull up to five tweets from the archive that include those words (if any) and run the tweet generation procedure with them. Return the result to the user and allow them to tweet it from their own twitter account. Could be fun for a few minutes and give some advertising for the app to their friends.

### Automa-whoever
Simply replace Trump as the source material. A procedural twitter bot for some of Twitter's other notorious users like Kanye West, Elon Musk (although now that might come with an outright ban), Lil B, or maybe the Pope, or anybody, really.

## Conclusion
With the rise of generative A.I. and the evolution of Twitter in to the no-fun-zone that is _X_, Automa-Trump is likely dead. I also got tired of getting emails from Firebase that I couldn't unsubscribe from telling me about their new cloud processor list and being charged $0.01 every month. But it was fun while it lasted.