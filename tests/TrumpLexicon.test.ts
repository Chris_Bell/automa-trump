import { expect } from 'chai';
import { describe, it, beforeEach } from 'mocha';

import TrumpLexicon from '../src/Models/TrumpLexicon';

describe('TrumpLexicon', () => {

    let lexicon: TrumpLexicon;

    const testData: string[] = [
        'The quick brown fox jumped over the lazy dog.',
        'You\'re all I ever wanted, you\'re all I ever needed, so tell me what to do, now, when I want you back.',
        'Shorts are comfortable and easy to wear!',
        'Run around town with the draco, bitch, D-R-A-C-O',
        'You have every right to be angry. We love you, you\'re very special, but you have to go home, now.',
        'Guys don’t know how to cook and wonder why they\'re lonely.',
        'If I see a single bad vibe in here I will lose my shit'
    ];

    const uniqueWords: string[] = Array.from(new Set(testData.join(' ').split(' ')));

    beforeEach(() => {
        lexicon = new TrumpLexicon();
    });

    it('should not create multiple keys for duplicate words', () => {
        let testString = 'The the the the the the';
        lexicon.add(testString);
        let map = lexicon.getMap();
        let keys = map.keys();

        expect(map.size).to.equal(2);
        expect(keys.next().value).to.equal('The');
        expect(keys.next().value).to.equal('the');
    });

    it('should create duplicate values for a given key', () => {
        let testString = 'The the the the the the';
        lexicon.add(testString);
        let map = lexicon.getMap();

        expect(map.get('the')).to.have.members(['the', 'the', 'the', 'the', 'EOF']);
    });

    it('should have one key for each unique word in testData', () => {
        lexicon.addAll(testData);
        let map = lexicon.getMap();

        expect(map.size).to.equal(uniqueWords.length);
    });

    it('should add EOF once for each string in testData', () => {
        lexicon.addAll(testData);
        let map = lexicon.getMap();
        let eofCount  = 0;

        for (let [key, val] of map) {
            if (val.includes('EOF')) { eofCount++; }
        }

        expect(eofCount).to.equal(testData.length);
    });

    it('should not add a key for EOF', () => {
        lexicon.addAll(testData);
        let map = lexicon.getMap();

        expect(map.has('EOF')).to.be.false;
    });

    it('should have the same number of startingWords as source strings', () => {
        lexicon.addAll(testData);
        const startingWords = lexicon.getStartingWords();

        expect(startingWords.length).to.equal(testData.length);
    });

    it('should have the same result whether calling addAll() or add() one at a time', () => {
        lexicon.addAll(testData);
        const bulkMap = lexicon.getMap();

        lexicon = new TrumpLexicon();
        testData.forEach((item) => lexicon.add(item));
        const loopedMap = lexicon.getMap();

        expect(JSON.stringify(bulkMap)).to.equal(JSON.stringify(loopedMap));
    });

});