import ITweet from '../Interfaces/ITweet';

export default class TrumpTweet implements ITweet {
    text: string;
    date: Date;
    isRetweet: boolean;
    isDeleted: boolean;
    device: string;
    favorites: number;
    retweets: number;
    isFlagged: boolean;

    constructor(firestoreData: any) {
        this.text = firestoreData.text;
        this.isRetweet = firestoreData.isRetweet;
        this.isDeleted = firestoreData.isDeleted;
        this.device = firestoreData.device;
        this.favorites = firestoreData.favorites;
        this.retweets = firestoreData.retweets;
        this.date = new Date(firestoreData.date.toDate());
        this.isFlagged = firestoreData.isFlagged;
    }
}