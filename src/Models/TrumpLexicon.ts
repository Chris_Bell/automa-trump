export default class TrumpLexicon {

    private wordMap = new Map<string, string[]>();
    private startingWords: string[] = [];

    public constructor(textToMap?: string[] | string) {
        if (textToMap) {
            if (textToMap instanceof Array) {
                this.addAll(textToMap);
            } else {
                this.add(textToMap);
            }
        }
    }

    public add(textToMap: string) {
        const words = textToMap.split(' ');

        this.startingWords.push(words[0]);

        for (let index = 0; index < words.length; index++) {
            const currentWord = words[index];
            let nextWord = words[index + 1];

            if (!nextWord) {
                nextWord = 'EOF';
            }

            if (this.wordMap.has(currentWord)) {
                this.wordMap.get(currentWord).push(nextWord);
            } else {
                this.wordMap.set(currentWord, [nextWord]);
            }
        }
    }

    public addAll(textToMap: string[]) {
        textToMap.forEach((item) => this.add(item));
    }

    public getMap() {
        return this.wordMap;
    }

    public getStartingWords() {
        return this.startingWords;
    }

    public getStartingWord() {
        return this.getRandomArrayElement(this.startingWords);
    }

    public getNextWord(currentWord: string) {
        let nextWords = this.wordMap.get(currentWord);
        return this.getRandomArrayElement(nextWords);
    }

    public print() {
        for (const [key, value] of this.wordMap.entries()) {
            console.log(key, value);
        }
    }

    private randomNumberInRange(length: number) {
        return Math.floor(Math.random() * length + 1) - 1;
    }

    private getRandomArrayElement(array: string[]) {
        return array[(this.randomNumberInRange(array.length))];
    }
}