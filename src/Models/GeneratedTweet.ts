import ITweet from '../Interfaces/ITweet';
import TrumpTweet from './TrumpTweet';

export default class GeneratedTweet implements ITweet {
    text: string;
    author: string;
    date: any;
    utcDate: string;
    sourceTweets: TrumpTweet[];
}