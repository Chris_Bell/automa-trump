import TrumpTweet from '../Models/TrumpTweet';

export function makeTweets(rawTweets: any[]): TrumpTweet[] {
    return rawTweets.map((rawTweet) => new TrumpTweet(rawTweet));
}
