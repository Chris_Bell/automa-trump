
import { makeTweets } from './Factories/TrumpTweetFactory';
import { generateText } from './Services/TweetGeneratorService';
import { getMostRecentTrumpTweets } from './Services/FirestoreService';
import TrumpLexicon from './Models/TrumpLexicon';
import { filterTweets } from './Services/TweetFilterService';

let testArray = ['Sleepy Joe Biden is the worst &amp; https://D928dJ2jds30.com.', ' &amp; Can\'t we just throw out all the blacks? https://D928dJ2jds30.com', 'Make America Elect Me Again https://D928dJ2jds30.com', ' https://D928dJ2jds30.com Joe Biden &amp; Kamala Harris are selling this country to the socialists' ];

// let testLexicon = new TrumpLexicon();
// testLexicon.addAll(testArray);
// const generateTweetText = generateText(testLexicon);
// console.log(generateTweetText);

getMostRecentTrumpTweets()
    .then((response) => {
        const tweetArray = makeTweets(response);
        filterTweets(tweetArray);
        const lexicon = new TrumpLexicon(tweetArray.map((tweet) => tweet.text));
        const generateTweetText = generateText(lexicon);
        console.log(generateTweetText);
    });