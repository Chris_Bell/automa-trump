export default interface ITweet {
    text: string;
    date: Date;
}