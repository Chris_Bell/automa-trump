import * as admin from 'firebase-admin';
import GeneratedTweet from '../Models/GeneratedTweet';
import TrumpTweet from '../Models/TrumpTweet';
let creds = require('../../credentials.json');

admin.initializeApp({
    credential: admin.credential.cert(creds),
    databaseURL: 'https://automa-trump.firebaseio.com'
});
const db = admin.firestore();

const trumpTweetsCollectionName = 'trump-tweets';
const generatedTweetsCollectionName = 'generated-tweets';

const trumpTweetsCollection = db.collection(trumpTweetsCollectionName);
const generatedTweetsCollection = db.collection(generatedTweetsCollectionName);

export async function getMostRecentTrumpTweets(numberToGet: number | string = 100) {
    try {
        const snapshot = await trumpTweetsCollection.orderBy('date', 'desc').limit(Number(numberToGet)).get();
        return snapshot.docs.map((doc) => doc.data());
    }
    catch (error) {
        console.error('FirestoreService.getMostRecentTweets failed: ', error);
    }
}

export async function getMostRecentGeneratedTweets(numberToGet: number | string = 100) {
    try {
        const snapshot = await generatedTweetsCollection.orderBy('date', 'desc').limit(Number(numberToGet)).get();
        return snapshot.docs.map((doc) => doc.data());
    }
    catch (error) {
        console.error('FirestoreService.getMostRecentGeneratedTweets failed: ', error);
    }
}

export async function getSizeOfTrumpTweetCollection() {
    try {
        const snapshot = await trumpTweetsCollection.get();
        return snapshot.size;
    }
    catch (error) {
        console.error('FirestoreService.getSizeOfTweetCollection failed: ', error);
    }
}

export async function saveGeneratedTweet(tweetText: string, sourceTweets: TrumpTweet[], author: string = '@AutomaTrump') {
    try {

        if (!(sourceTweets?.length > 0)) {
            throw ('No sourceTweets');
        }

        const generatedTweet: GeneratedTweet = {
            text: tweetText,
            author,
            date: admin.firestore.FieldValue.serverTimestamp(),
            utcDate: new Date().toUTCString(),
            sourceTweets: JSON.parse(JSON.stringify(sourceTweets))  // firestore can't serialize them on its own for some reason
        };

        return generatedTweetsCollection.add(generatedTweet);
    }
    catch (error) {
        console.error('FirestoreService.saveGeneratedTweet failed: ', error);
    }
}