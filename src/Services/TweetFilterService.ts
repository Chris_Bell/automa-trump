import TrumpTweet from '../Models/TrumpTweet';

export function filterTweets(tweetArray: TrumpTweet[]) {
    tweetArray = filterRetweets(tweetArray);
    tweetArray = filterStaffTweets(tweetArray);
    return tweetArray;
}

function filterRetweets(tweetArray: TrumpTweet[]) {
    return tweetArray.filter((tweet) =>
        !tweet.isRetweet);
}

function filterStaffTweets(tweetArray: TrumpTweet[]) {
    return tweetArray.filter((tweet) =>
        !tweet.text.includes('http'));
}