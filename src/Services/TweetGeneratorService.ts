import TrumpLexicon from '../Models/TrumpLexicon';
import { fixNonWords } from './TweetTextCleaner';

export const MAX_TWEET_LENGTH = 280;

export function generateText(lexicon: TrumpLexicon) {
    let newText: string = lexicon.getStartingWord();
    let currentWord: string = newText;
    while (newText.length < MAX_TWEET_LENGTH) {
        currentWord = lexicon.getNextWord(currentWord);

        if (currentWord == 'EOF') {
            break;
        }

        if (newText.length + currentWord.length + 1 > MAX_TWEET_LENGTH) {   // + 1 to account for whitespace
            break;
        }
        newText += ' ' + currentWord;

        if (wordEndsInPunctuation(currentWord)) {
            break;
        }
    }

    newText = fixNonWords(newText);
    return newText.trim();
}

function wordEndsInPunctuation(word: string): boolean {
    return word.endsWith('.') ||
           word.endsWith('?') ||
           word.endsWith('!');
}